package com.example.taskboard.demo.service;

import com.example.taskboard.demo.dto.LoginDto;

import com.example.taskboard.demo.dto.RegisterDto;
import com.example.taskboard.demo.exception.IllegalArgumentException;
import com.example.taskboard.demo.exception.NotFoundException;
import com.example.taskboard.demo.model.Role;
import com.example.taskboard.demo.model.User;
import com.example.taskboard.demo.repository.RoleRepository;
import com.example.taskboard.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    public User create(RegisterDto registerDto) {
        User userToRegister = new User(registerDto.getLogin(), registerDto.getEmail(), registerDto.getPassword());
        validateLogin(userToRegister.getLogin());
        validateEmail(userToRegister.getEmail());

        userToRegister.setPassword(bCryptPasswordEncoder.encode(userToRegister.getPassword()));

        return userRepository.save(userToRegister);
    }

    public User editUser(User user, Long id) {
        User existingUser = getId(id);
        validateLogin(user.getLogin());
        validateEmail(user.getEmail());

        existingUser.setLogin(user.getLogin());
        existingUser.setEmail(user.getEmail());
        existingUser.setPassword(user.getPassword());

        return userRepository.save(existingUser);
    }

    private void validateLogin(String login) {
        if (userRepository.existsByLogin(login)) {
            throw new IllegalArgumentException(String.format(" Login: %s already exists", login));
        }
    }

    private void validateEmail(String email) {
        if (userRepository.existsByEmail(email)) {
            throw new IllegalArgumentException(String.format("E-mail: %s already exists", email));
        }
    }

    public User getId(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("User with id %d not found", id)));
    }

    public Optional<User> getUserWithLoginAndPassword(LoginDto loginDto) {
        // z bazy danych wyciągam użytkownika o loginie
        Optional<User> userFromDB = userRepository.findByLogin(loginDto.getLogin());
        if (userFromDB.isPresent()) { // jeśli taki istnieje
            User user = userFromDB.get(); // sprawdzam jego hasło (niżej)
            if (bCryptPasswordEncoder.matches(loginDto.getPassword(), user.getPassword())) {
                return userFromDB; // jeśli hasło się zgadza, to zwracam użytkownika
            }
        }
        return Optional.empty();
    }
}
