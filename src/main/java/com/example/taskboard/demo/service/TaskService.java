package com.example.taskboard.demo.service;

import com.example.taskboard.demo.exception.NotFoundException;
import com.example.taskboard.demo.model.Task;
import com.example.taskboard.demo.model.TaskStatus;
import com.example.taskboard.demo.model.User;
import com.example.taskboard.demo.repository.TaskRepository;
import com.example.taskboard.demo.repository.TaskStatusRepository;
import com.example.taskboard.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TaskStatusRepository taskStatusRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    public Task create(Task task) {
        Optional<TaskStatus> openedStatus = taskStatusRepository.findByName("OPENED");
        task.setTaskStatuses(openedStatus.get());
        return taskRepository.save(task);
    }

    public Task editTask(Task task, Long id) {
        Task existingTask = getId(id);

        if (task.getTitle() == null) {
            existingTask.setTitle(existingTask.getTitle());
        } else
            existingTask.setTitle(task.getTitle());

        if (task.getDescription() == null) {
            existingTask.setDescription(existingTask.getDescription());
        } else
            existingTask.setDescription(task.getDescription());

        if (task.getDueDate() == null) {
            existingTask.setDueDate(existingTask.getDueDate());
        } else
            existingTask.setDueDate(task.getDueDate());

        existingTask.setUser(task.getUser());

        return taskRepository.save(existingTask);
    }

    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    public Task getId(Long id) {
        return taskRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Task with id %d not found", id)));
    }

    public void remove(Long id) {
        Optional<Task> taskToRemove = taskRepository.findById(id);
        if (!taskToRemove.isPresent()) {
            throw new NotFoundException(String.format("Task with id %d not found", id));
        } else {
            Task task = taskToRemove.get();
            taskRepository.delete(task);
        }
    }

    public Task assignUser(Long taskId, Long userId) {
        Optional<Task> taskToReasign = taskRepository.findById(taskId);
        if (!taskToReasign.isPresent()) {
            throw new NotFoundException(String.format("Task with id %d not found", taskId));
        } else {
            Task task = taskToReasign.get();
            task.setTaskStatuses(taskStatusRepository.findById(2L).get());
            task.setUser(userRepository.findById(userId).get());
            return taskRepository.save(task);
        }

    }
}
