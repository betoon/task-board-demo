package com.example.taskboard.demo;

import com.example.taskboard.demo.model.Task;
import com.example.taskboard.demo.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskboardApplication.class, args);

    }


}
