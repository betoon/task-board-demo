package com.example.taskboard.demo.dto;

import com.example.taskboard.demo.model.Task;
import com.example.taskboard.demo.model.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.font.FontRenderContext;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private String login;
    private String email;
    private List<Task> tasks;

    public static UserDto create(User user) {
        return new UserDto(user.getLogin(), user.getEmail(), user.getTasks());
    }
}
