package com.example.taskboard.demo.dto;

import com.example.taskboard.demo.model.Task;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private String title;
    private String description;
    private LocalDateTime addedTime;
    private LocalDate dueDate;

    public static TaskDto create(Task task) {
        return new TaskDto(task.getTitle(), task.getDescription(), task.getAddedTime(), task.getDueDate());
    }

}
