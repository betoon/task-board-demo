package com.example.taskboard.demo.controller;

import com.example.taskboard.demo.dto.UserDto;
import com.example.taskboard.demo.model.User;
import com.example.taskboard.demo.repository.UserRepository;
import com.example.taskboard.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

//    @RequestMapping(path = "/create", method = RequestMethod.POST)
//    @ResponseStatus(HttpStatus.CREATED)
//    public UserDto create(@Valid @RequestBody User user) {
//        return UserDto.create(userService.create(user));
//    }

    @RequestMapping(path = "/edit", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto editUser(@Valid @RequestBody User user, @RequestParam Long id) {
        return UserDto.create(userService.editUser(user, id));
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<User> usersList() {
        return userService.findAllUsers();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(@RequestParam Long id) {
        return UserDto.create(userService.getId(id));
    }
}
