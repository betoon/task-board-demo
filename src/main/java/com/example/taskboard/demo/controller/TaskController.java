package com.example.taskboard.demo.controller;


import com.example.taskboard.demo.dto.TaskDto;
import com.example.taskboard.demo.exception.NotFoundException;
import com.example.taskboard.demo.model.Task;

import com.example.taskboard.demo.model.User;
import com.example.taskboard.demo.repository.TaskRepository;
import com.example.taskboard.demo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/task/")
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    TaskRepository taskRepository;

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto create(@Valid @RequestBody Task task) {

        task.setAddedTime(LocalDateTime.now());

        return TaskDto.create(taskService.create(task));
    }

    @RequestMapping(path = "/edit", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDto editTask(@RequestBody Task task, @RequestParam Long id) {
        return TaskDto.create(taskService.editTask(task, id));
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Task> taskList() {
        return taskService.findAllTasks();
    }

    @RequestMapping(path = "/get", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TaskDto getTask(@RequestParam Long id) {
        return TaskDto.create(taskService.getId(id));
    }

    @RequestMapping(path = "/remove", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void removeTask(@RequestParam Long id) {
        taskService.remove(id);
    }

    @RequestMapping(path = "/assign", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public TaskDto assignUser(@RequestParam(name = "id") Long taskId, @RequestParam(name = "user") Long userId) {
        return TaskDto.create(taskService.assignUser(taskId, userId));
    }

}
