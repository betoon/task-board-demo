package com.example.taskboard.demo.controller;

import com.example.taskboard.demo.dto.AuthenticationDto;
import com.example.taskboard.demo.dto.LoginDto;
import com.example.taskboard.demo.dto.RegisterDto;
import com.example.taskboard.demo.model.Role;
import com.example.taskboard.demo.model.User;
import com.example.taskboard.demo.response.ResponseFactory;
import com.example.taskboard.demo.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.taskboard.demo.config.JWTFilter.ENCRYPTION_SECRET;

@RestController
@CrossOrigin
@RequestMapping(path = "/auth")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody RegisterDto registerDto) {
        User user = userService.create(registerDto);
        return ResponseFactory.ok();
    }

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody LoginDto loginDto) {
        Optional<User> optionalUser = userService.getUserWithLoginAndPassword(loginDto);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            byte[] apiPassword = DatatypeConverter.parseBase64Binary(ENCRYPTION_SECRET);
            Key key = new SecretKeySpec(apiPassword, signatureAlgorithm.getJcaName());

            String token = Jwts.builder()
                    .setSubject(user.getLogin()).setIssuedAt(new Date())
                    .claim("roles", translateRoles(user.getRoleSet()))
                    .signWith(signatureAlgorithm, key)
                    .compact();
            return ResponseFactory.ok(new AuthenticationDto(user.getLogin(), user.getId(), token));
        }
        return ResponseFactory.badRequest();
    }

    private Set<String> translateRoles(List<Role> roles) {
        return roles.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }

}
