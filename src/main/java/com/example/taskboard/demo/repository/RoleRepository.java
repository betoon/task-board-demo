package com.example.taskboard.demo.repository;

import com.example.taskboard.demo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
