package com.example.taskboard.demo.repository;

import com.example.taskboard.demo.dto.UserDto;
import com.example.taskboard.demo.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long Id);
    boolean existsByLogin(String login);
    boolean existsByEmail(String email);


    Optional<User> findByLogin(String login);
}
