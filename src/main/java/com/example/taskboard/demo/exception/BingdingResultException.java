package com.example.taskboard.demo.exception;


import lombok.Getter;
import org.springframework.validation.BindingResult;

public class BingdingResultException extends RuntimeException {

    @Getter
    private final BindingResult bindingResult;

    public BingdingResultException(BindingResult bindingResult){
        this.bindingResult = bindingResult;
    }
}
