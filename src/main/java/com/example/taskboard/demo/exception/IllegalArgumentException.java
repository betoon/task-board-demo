package com.example.taskboard.demo.exception;

public class IllegalArgumentException extends RuntimeException {

    public IllegalArgumentException(String message){
        super(message);
    }
}
