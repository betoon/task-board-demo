package com.example.taskboard.demo.components;


import com.example.taskboard.demo.model.Task;
import com.example.taskboard.demo.model.TaskStatus;
import com.example.taskboard.demo.repository.TaskStatusRepository;
import com.example.taskboard.demo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {
    private TaskService taskService;
    private TaskStatusRepository taskStatusRepository;


    @Autowired
    public DataInitializer(TaskService taskService, TaskStatusRepository taskStatusRepository) {
        this.taskService = taskService;
        this.taskStatusRepository = taskStatusRepository;
//        initialize();
    }

//    private void initialize() {
//        TaskStatus openTask = new TaskStatus("OPENED");
//        TaskStatus assignedTask = new TaskStatus("ASSIGNED");
//        TaskStatus closedTask = new TaskStatus("CLOSED");
//        TaskStatus blockedTask = new TaskStatus("BLOCKED");
//
//        taskStatusRepository.save(openTask);
//        taskStatusRepository.save(assignedTask);
//        taskStatusRepository.save(closedTask);
//        taskStatusRepository.save(blockedTask);
//    }

}
